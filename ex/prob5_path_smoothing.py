# -----------
# User Instructions
#
# Define a function smooth that takes a path as its input
# (with optional parameters for weight_data, weight_smooth,
# and tolerance) and returns a smooth path. The first and
# last points should remain unchanged.
#
# Smoothing should be implemented by iteratively updating
# each entry in newpath until some desired level of accuracy
# is reached. The update should be done according to the
# gradient descent equations given in the instructor's note
# below (the equations given in the video are not quite
# correct).
# -----------

from copy import deepcopy

# thank you to EnTerr for posting this on our discussion forum
def printpaths(path,newpath):
    for old,new in zip(path,newpath):
        print '['+ ', '.join('%.3f'%x for x in old) + \
               '] -> ['+ ', '.join('%.3f'%x for x in new) +']'

# Don't modify path inside your function.
path = [[0, 0],
        [0, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [4, 2],
        [4, 3],
        [4, 4]]

def smooth(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.000001):

    # Make a deep copy of path into newpath
    newpath = deepcopy(path)

    #######################
    ### ENTER CODE HERE ###
    #######################
    def iterate(path, newpath, weight_data, weight_smooth, tolerance):

        dim = len(path)

        totalDelta = 0
        for i in range(dim):
            if (i == 0 or i == dim - 1):
                continue
            for j in range(len(path[0])):
                x = path[i][j]
                y = newpath[i][j]
                y_m1 = newpath[i - 1][j]
                y_p1 = newpath[i + 1][j]

                delta = weight_data * (x - y) + weight_smooth * (y_p1 + y_m1 - 2 * y)
                totalDelta += abs(delta)

                newpath[i][j] += delta

        if totalDelta < tolerance:
            return newpath
        else:
            return iterate(path, newpath, weight_data, weight_smooth, tolerance)


    newpath = iterate(path, newpath, weight_data, weight_smooth, tolerance)

    return newpath # Leave this line for the grader!

printpaths(path,smooth(path))
