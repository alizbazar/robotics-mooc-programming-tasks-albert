P_f = 0.001
P_lie = 0.1

P_b_while_f = (1-P_lie)
Pt_f_while_b = P_b_while_f * P_f

P_not_f = 1 - P_f
P_b_while_not_f = P_lie

Pt_not_f_while_b = P_b_while_not_f * P_not_f

norm = Pt_f_while_b + Pt_not_f_while_b

P_f_while_b = Pt_f_while_b / norm

P_not_f_while_b = Pt_not_f_while_b / norm

print Pt_f_while_b
print Pt_not_f_while_b
print P_f_while_b
print P_not_f_while_b