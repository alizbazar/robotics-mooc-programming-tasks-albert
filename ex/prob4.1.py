# ----------
# User Instructions:
#
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's
# optimal path to the position specified in goal;
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a
# right turn.

forward = [[-1,  0], # go up
           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # go right
forward_name = ['up', 'left', 'down', 'right']

# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]

init = [4, 3, 0] # given in the form [row,col,direction]
                 # direction = 0: up
                 #             1: left
                 #             2: down
                 #             3: right

goal = [2, 0] # given in the form [row,col]

cost = [2, 1, 20] # cost has 3 values, corresponding to making
                  # a right turn, no turn, and a left turn

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------

def optimum_policy2D(grid,init,goal,cost):

    maxH = 999
    gridH = [[[maxH for row in range(len(grid[0]))] for col in range(len(grid))],
             [[maxH for row in range(len(grid[0]))] for col in range(len(grid))],
             [[maxH for row in range(len(grid[0]))] for col in range(len(grid))],
             [[maxH for row in range(len(grid[0]))] for col in range(len(grid))]]

    policy = [[[' ' for row in range(len(grid[0]))] for col in range(len(grid))],
             [[' ' for row in range(len(grid[0]))] for col in range(len(grid))],
             [[' ' for row in range(len(grid[0]))] for col in range(len(grid))],
             [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]]

    x = goal[0]
    y = goal[1]
    h = 0
    rows = len(grid)
    cols = len(grid[0])

    next = []
    for i in range(len(forward)):
        gridH[i][x][y] = h
        policy[i][x][y] = '*'
        next.append([h, x, y, i])

    while len(next) != 0:
        next.sort()
        current = next.pop(0)

        h = current[0]
        x = current[1]
        y = current[2]
        theta = current[3]

        # reverse the action by subtracting orientation step
        delta = forward[theta]
        x2 = x - delta[0]
        y2 = y - delta[1]

        if (x2 >= 0 and y2 >= 0 and x2 < rows and y2 < cols and grid[x2][y2] != 1):

            for j in range(len(action)):
                theta2 = (theta - action[j]) % 4
                h2 = h + cost[j]
                if (h2 < gridH[theta2][x2][y2]):
                    next.append([h2, x2, y2, theta2])
                    gridH[theta2][x2][y2] = h2
                    policy[theta2][x2][y2] = j

    policy2D = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]

    currentState = init
    policy2D[goal[0]][goal[1]] = '*'
    while (currentState[0] != goal[0] or currentState[1] != goal[1]):
        currentAction = policy[currentState[2]][currentState[0]][currentState[1]]

        policy2D[currentState[0]][currentState[1]] = action_name[currentAction]

        orientation = forward[currentState[2]]
        theta = (currentState[2] + action[currentAction]) % 4
        delta = forward[theta]

        #take the action
        x = currentState[0] + delta[0]
        y = currentState[1] + delta[1]

        currentState = [x, y, theta]

    return policy2D

#pol = optimum_policy2D(grid,init,goal,cost)
#for i in range(len(pol)):
#    print pol[i]