# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position.
#
# ----------
# GRADING
#
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random
import numpy as np
from numpy.linalg import inv

def calculate_circle(measurements):
    # least square circle:
    # http://www.had2know.com/academics/best-fit-circle-least-squares.html
    x_pow_2 = 0
    xy = 0
    x_sum = 0
    y_pow_2 = 0
    y_sum = 0
    n = len(measurements)
    x_pyth = 0
    y_pyth = 0
    pyth = 0
    for i in range(n):
        measurement = measurements[i]
        x = measurement[0]
        y = measurement[1]
        x_sum += x
        y_sum += y
        x_pow_2 += x**2
        y_pow_2 += y**2
        xy += x*y
        x_pyth += x * (x**2 + y**2)
        y_pyth += y * (x**2 + y**2)
        pyth += (x**2 + y**2)

    multiples = np.matrix([[x_pow_2, xy, x_sum],
                        [xy, y_pow_2, y_sum],
                        [x_sum, y_sum, n]])
    equals = np.matrix([[x_pyth], [y_pyth], [pyth]])
    ABC = inv(multiples).dot(equals)

    A = float(ABC[0][0])
    B = float(ABC[1][0])
    C = float(ABC[2][0])
    h = A / 2
    k = B / 2
    r = sqrt(4 * C + A**2 + B**2) / 2

    return [h, k, r]

def in_pi_range(val):
    if (val > pi):
        val -= 2 * pi
    elif (val <= -pi):
        val += 2 * pi
    return val

def x_from_past_z(measurements):
    [h, k, r] = calculate_circle(measurements)

    # get previous measurements
    measurement = measurements[len(measurements) - 1]
    old_measurement = measurements[len(measurements) - 2]

    dx_old = old_measurement[0] - h
    dy_old = old_measurement[1] - k
    theta_old = atan2(dy_old, dx_old)

    dx = measurement[0] - h
    dy = measurement[1] - k
    theta = atan2(dy, dx)

    theta_delta = theta - theta_old

    theta_delta = in_pi_range(theta_delta)

    return [h, k, r, theta, theta_delta]

def jacobian_state_per_x(measurements):
    ref_x = matrix([x_from_past_z(measurements)])
    # dx
    measurements[len(measurements) - 1][0] += 0.01
    F_dx = matrix([x_from_past_z(measurements)]) - ref_x
    for i in range(F_dx.dimy):
        F_dx.value[0][i] *= 100
    measurements[len(measurements) - 1][0] -= 0.01

    # dy
    measurements[len(measurements) - 1][1] += 0.01
    F_dy = matrix([x_from_past_z(measurements)]) - ref_x
    for i in range(F_dy.dimy):
        F_dy.value[0][i] *= 100
    measurements[len(measurements) - 1][1] -= 0.01

    return matrix([F_dx.value[0], F_dy.value[0]]).transpose()

def z_from_x(x):
    [h, k, r, theta, theta_delta] = x
    z_x = h + r * cos(theta)
    z_y = k + r * sin(theta)
    return matrix([[z_x], [z_y]])

def next_x(x):
    theta_delta = x.value[4][0]
    x.value[3][0] = in_pi_range(x.value[3][0] + theta_delta)
    return x

def update_mean_x(x_new, trace):
    mean_x, n, M2 = trace
    # NOTE: this is in essence the length of memory
    # if (n < 10):
    n += 1
    delta = matrix([x_new]).transpose() - mean_x
    delta.value[3][0] = in_pi_range(delta.value[3][0])
    prev_delta = matrix(delta.value[:])
    for i in range(delta.dimx):
        delta.value[i][0] = delta.value[i][0] / n
    # increase whe "weight" of the last angular location
    # delta.value[3][0] = delta.value[3][0] * n / 3

    mean_x = mean_x + delta

    new_delta = matrix([x_new]).transpose() - mean_x
    new_delta.value[3][0] = in_pi_range(new_delta.value[3][0])
    M2 = M2 + prev_delta * new_delta
    return mean_x, n, M2

# This is the function you have to write. Note that measurement is a
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    if (OTHER == None):
        # Init OTHER to hold all variables
        measurements = [measurement]

        initial_theta = 0.

        x = matrix([[0.], [0.], [1.], [initial_theta], [0.]]) # initial state (location and velocity)

        mean_x = matrix([[0.,0.,0.,0.,0.]]).transpose()
        n = 0
        M2 = 0.
        trace = mean_x, n, M2

        OTHER = measurements, x, trace

        xy_estimate = (measurement[0], measurement[1])
        return xy_estimate, OTHER


    measurements, x, trace = OTHER

    # append current measurement
    measurements.append(measurement)

    if (len(measurements) < 3):
        xy_estimate = (measurement[0], measurement[1])

        # store latest measurements to OTHER
        OTHER = measurements, x, trace

        return xy_estimate, OTHER
    elif (len(measurements) > 20):
        measurements.pop(0)

    # GO AHEAD with calculation
    x_new = x_from_past_z(measurements)
    trace = update_mean_x(x_new, trace)
    x, n, M2 = trace
    # x_old = x
    # x = matrix([x_new]).transpose()
    # y = x - x_old
    # print "y:", y

    # print "m:", measurement, "-> x0", h, "y0", k, "r", r, "theta", degrees(theta), "theta_delta", degrees(theta_delta)
    # print "z_from_x:", z_from_x([h, k, r, theta, theta_delta])
    # print "x:", x


    # prediction
    # TODO: replace F * x by next state calculation from x
    x = next_x(x) # predict next x
    # save new predicted x to trace
    trace = x, n, M2

    xy_estimate = z_from_x(x.transpose().value[0]).transpose().value[0]

    OTHER = measurements, x, trace

    # You must return xy_estimate (x, y), and OTHER (even if it is None)
    # in this order for grading purposes.
    # xy_estimate = (3.2, 9.1)
    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any
# information that you want.
# def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
#     localized = False
#     distance_tolerance = 0.01 * target_bot.distance
#     ctr = 0
#     # if you haven't localized the target bot, make a guess about the next
#     # position, then we move the bot and compare your guess to the true
#     # next position. When you are close enough, we stop checking.
#     while not localized and ctr <= 1000:
#         ctr += 1
#         measurement = target_bot.sense()
#         position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
#         target_bot.move_in_circle()
#         true_position = (target_bot.x, target_bot.y)
#         error = distance_between(position_guess, true_position)
#         if error <= distance_tolerance:
#             print "You got it right! It took you ", ctr, " steps to localize."
#             localized = True
#         if ctr == 1000:
#             print "Sorry, it took you too many steps to localize the target."
#     return localized

# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER
    return xy_estimate, OTHER



def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        #End of Visualization
    return localized



# This is how we create a target bot. Check the robot.py file to understand
# How the robot class behaves.
test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)

demo_grading(estimate_next_pos, test_target)




