# ----------
# Part Four
#
# Again, you'll track down and recover the runaway Traxbot.
# But this time, your speed will be about the same as the runaway bot.
# This may require more careful planning than you used last time.
#
# ----------
# YOUR JOB
#
# Complete the next_move function, similar to how you did last time.
#
# ----------
# GRADING
#
# Same as part 3. Again, try to catch the target in as few steps as possible.

from robot import *
from math import *
from matrix import *
import random
import numpy as np
from numpy.linalg import inv

def x_from_past_z(measurement, old_measurement):
    s_dx = measurement[0] - old_measurement[0]
    s_dy = measurement[1] - old_measurement[1]
    theta = atan2(s_dy, s_dx)
    s = sqrt(s_dx**2 + s_dy**2)

    return matrix([[measurement[0]], [measurement[1]], [s], [theta], [0]])

def next_x(x):
    [x0, y0, s, theta, theta_delta] = x.transpose().value[0]
    return x + matrix([[s*cos(theta + theta_delta)], [s*sin(theta + theta_delta)], [0], [theta_delta], [0]])

def get_jacobian_F(x):
    [x0, y0, s, theta, theta_delta] = x.transpose().value[0]

    theta_avg = theta - theta_delta / 2

    return matrix([[1., 0., cos(theta_avg), -s*sin(theta_avg), 0.],
                   [0., 1., sin(theta_avg), s*cos(theta_avg), 0.],
                   [0., 0., 1., 0., 0.],
                   [0., 0., 0., 1., 1.],
                   [0., 0., 0., 0., 1.]])

def in_pi_range(val):
    if (val > pi):
        val -= 2 * pi
    elif (val <= -pi):
        val += 2 * pi
    return val

    return matrix([[1., 0., cos(theta_avg), -s*sin(theta_avg), 0.],
                   [0., 1., sin(theta_avg), s*cos(theta_avg), 0.],
                   [0., 0., 1., 0., 0.],
                   [0., 0., 0., 1., 1.],
                   [0., 0., 0., 0., 1.]])

def get_closest_feasible_target(hunter_position, x, max_distance):
    n = 0
    x_predicted = x
    while True and n < 100:
        x0 = x_predicted.value[0][0]
        y0 = x_predicted.value[1][0]
        dx = x0 - hunter_position[0]
        dy = y0 - hunter_position[1]
        s = sqrt(dx**2 + dy**2)
        n += 1
        if (s < max_distance * n):
            return s, atan2(dy, dx)
        x_predicted = next_x(x)
    return 0, 0


def next_move(hunter_position, hunter_heading, measurement, max_distance, OTHER = None):
    # This function will be called after each time the target moves.

    if (OTHER == None):
        # Init OTHER to hold all variables

        x = matrix([[measurement[0]], [measurement[1]], [0.], [0.], [0.]]) # initial state (location and velocity)
        u = matrix([[]]) # external motion
        u.zero(5, 1)

        ### fill this in: ###
        # initial uncertainty
        P = matrix([[100., 0., 0., 0., 0.],
                    [0., 100., 0., 0., 0.],
                    [0., 0., 1., 0., 0.],
                    [0., 0., 0., 1., 0.],
                    [0., 0., 0., 0., 1000.]])

        # measurement uncertainty
        R = matrix([[0.01, 0.], [0., 0.01]])

        # identity matrix
        I = matrix([[]])
        I.identity(5)

        OTHER = x, u, P, R, I

        distance, theta_target = get_closest_feasible_target(hunter_position, x, max_distance)
        theta_turn = in_pi_range(theta_target - hunter_heading)
        distance_drive = min(distance, max_distance)

        return theta_turn, distance_drive, OTHER

    x, u, P, R, I = OTHER

    if (x.value[2][0] == 0):
        x = x_from_past_z(measurement, [x.value[0][0], x.value[1][0]])

    # measurement update
    Z = matrix([[measurement[0], measurement[1]]])
    y = Z.transpose() - matrix([[x.value[0][0]], [x.value[1][0]]]) # difference between measurements and assumed measurements

    # H is Jacobian (partial derivatives) of Z(k) over X(k) at X(k)
    H = matrix([[1, 0, 0, 0, 0],
                [0, 1, 0, 0, 0]])
    S = H * P * H.transpose() + R
    K = P * H.transpose() * matrix(inv(np.matrix(S.value)).tolist()) # K is G in WLU.edu

    # print "y:", y
    # print "x:", x

    x = x + (K * y) # correction to state
    P = (I - (K * H)) * P # correction to uncertainty

    # print "x':", x
    # print "P:"
    # print P.value[0]
    # print P.value[1]
    # print P.value[2]
    # print P.value[3]
    # print P.value[4]

    # prediction
    # TODO: replace F * x by next state calculation from x
    x = next_x(x) + u # predict next x

    # F is Jacobian (partial derivatives) of X(k) per X(k-1)
    F = get_jacobian_F(x)
    P = F * P * F.transpose()

    OTHER = x, u, P, R, I

    distance, theta_target = get_closest_feasible_target(hunter_position, x, max_distance)
    theta_turn = in_pi_range(theta_target - hunter_heading)
    distance_drive = min(distance, max_distance)



    # The OTHER variable is a place for you to store any historical information about
    # the progress of the hunt (or maybe some localization information). Your return format
    # must be as follows in order to be graded properly.
    return theta_turn, distance_drive, OTHER

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we
    will grade your submission."""
    max_distance = 0.98 * target_bot.distance # 0.98 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0

    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:

        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)

        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_circle()

        ctr += 1
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught



def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def naive_next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER):
    """This strategy always tries to steer the hunter directly towards where the target last
    said it was and then moves forwards at full speed. This strategy also keeps track of all
    the target measurements, hunter positions, and hunter headings over time, but it doesn't
    do anything with that information."""
    if not OTHER: # first time calling this function, set up my OTHER variables.
        measurements = [target_measurement]
        hunter_positions = [hunter_position]
        hunter_headings = [hunter_heading]
        OTHER = (measurements, hunter_positions, hunter_headings) # now I can keep track of history
    else: # not the first time, update my history
        OTHER[0].append(target_measurement)
        OTHER[1].append(hunter_position)
        OTHER[2].append(hunter_heading)
        measurements, hunter_positions, hunter_headings = OTHER # now I can always refer to these variables

    heading_to_target = get_heading(hunter_position, target_measurement)
    heading_difference = heading_to_target - hunter_heading
    turning =  heading_difference # turn towards the target
    distance = max_distance # full speed ahead!
    return turning, distance, OTHER

target = robot(0.0, 10.0, 0.0, 2*pi / 30, 1.5)
measurement_noise = .05*target.distance
target.set_noise(0.0, 0.0, measurement_noise)

hunter = robot(-10.0, -10.0, 0.0)

print demo_grading(hunter, target, next_move)





