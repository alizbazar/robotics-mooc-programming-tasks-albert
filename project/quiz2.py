# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position.
#
# ----------
# GRADING
#
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random
import numpy as np
from numpy.linalg import inv

def calculate_circle(measurements):
    # least square circle:
    # http://www.had2know.com/academics/best-fit-circle-least-squares.html
    x_pow_2 = 0
    xy = 0
    x_sum = 0
    y_pow_2 = 0
    y_sum = 0
    n = len(measurements)
    x_pyth = 0
    y_pyth = 0
    pyth = 0
    for i in range(n):
        measurement = measurements[i]
        x = measurement[0]
        y = measurement[1]
        x_sum += x
        y_sum += y
        x_pow_2 += x**2
        y_pow_2 += y**2
        xy += x*y
        x_pyth += x * (x**2 + y**2)
        y_pyth += y * (x**2 + y**2)
        pyth += (x**2 + y**2)

    multiples = np.matrix([[x_pow_2, xy, x_sum],
                        [xy, y_pow_2, y_sum],
                        [x_sum, y_sum, n]])
    equals = np.matrix([[x_pyth], [y_pyth], [pyth]])
    ABC = inv(multiples).dot(equals)

    A = float(ABC[0][0])
    B = float(ABC[1][0])
    C = float(ABC[2][0])
    h = -A / 2
    k = -B / 2
    r = sqrt(4 * C + A**2 + B**2) / 2

    return [h, k, r]

def in_pi_range(val):
    if (val > pi):
        val -= 2 * pi
    elif (val <= -pi):
        val += 2 * pi
    return val

# This is the function you have to write. Note that measurement is a
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    if (OTHER == None):
        # Init OTHER to hold all variables
        measurements = [measurement]

        dt = 1
        initial_theta = 0

        x = matrix([[initial_theta], [0.]]) # initial state (location and velocity)
        u = matrix([[0.], [0.]]) # external motion

        ### fill this in: ###
        # initial uncertainty
        P = matrix([[0., 0.],
                    [0., 1000.]])

        # next state function
        F = matrix([[1., 0.],
                    [0., dt]])

        # measurement function
        H = matrix([[1., 0.],
                    [0., 1.]])

        # measurement uncertainty
        R = matrix([[0.1, 0.],
                    [0., 0.1]])

        # identity matrix
        I = matrix([[1., 0.],
                    [0., 1.]])

        OTHER = measurements, dt, x, u, P, F, H, R, I

        xy_estimate = (measurement[0], measurement[1])
        return xy_estimate, OTHER


    measurements, dt, x, u, P, F, H, R, I = OTHER
    # get previous measurement for reference
    old_measurement = measurements[len(measurements) - 1]

    # append current measurement
    measurements.append(measurement)

    if (len(measurements) < 3):
        xy_estimate = (measurement[0], measurement[1])

        # store latest measurements to OTHER
        OTHER[0] = measurements

        return xy_estimate, OTHER

    # Make sure only N number of last measurements are stored
    # (pop oldest)
    if (len(measurements) > 20):
        measurements.pop(0)


    # GO AHEAD with calculation
    [h, k, r] = calculate_circle(measurements)

    dx_old = old_measurement[0] - h
    dy_old = old_measurement[1] - k
    theta_old = atan2(dy_old / dx_old)

    dx = measurement[0] - h
    dy = measurement[1] - k
    theta = atan2(dy / dx)

    theta_delta = theta - theta_old

    theta_delta = in_pi_range(theta_delta)


    # measurement update
    Z = matrix([[theta, theta_delta]])
    y = Z.transpose() - (H * x) # error

    # make sure error is in (-pi, pi] range
    for i in range(y.dimy):
        y.value[i][0] = in_pi_range(y.value[i][0])

    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)
    P = (I - (K * H)) * P

    # prediction
    x = (F * x) + u

    # TODO: make sure in range
    for i in range(x.dimy):
        x.value[i][0] = in_pi_range(x.value[i][0])

    P = F * P * F.transpose()


    OTHER = measurements, dt, x, u, P, F, H, R, I

    # calculate result based on angle
    xy_estimate = []
    xy_estimate[0] = h + r * cos(x.value[0][0])
    xy_estimate[1] = k + r * sin(x.value[0][0])

    # You must return xy_estimate (x, y), and OTHER (even if it is None)
    # in this order for grading purposes.
    # xy_estimate = (3.2, 9.1)
    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any
# information that you want.
def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
    return localized

# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER
    return xy_estimate, OTHER







# This is how we create a target bot. Check the robot.py file to understand
# How the robot class behaves.
test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)

demo_grading(estimate_next_pos, test_target)




