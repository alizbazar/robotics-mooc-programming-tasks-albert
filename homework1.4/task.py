colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

measurements = ['green', 'green', 'green', 'green', 'green']


motions = [[0, 0], [0, 1], [1, 0], [1, 0], [0, 1]]

sensor_right = 0.7

p_move = 0.8

def show(p):
    for i in range(len(p)):
        print p[i]

def localize(colors,measurements,motions,sensor_right,p_move):
    # initializes p to a uniform distribution over a grid of the same dimensions as colors
    pinit = 1.0 / float(len(colors)) / float(len(colors[0]))
    p = [[pinit for row in range(len(colors[0]))] for col in range(len(colors))]


    for i in range(len(motions)):
        p = move(p, motions[i], p_move)
        p = measure(p, measurements[i], sensor_right, colors)

    # >>> Insert your code here <<<

    return p

def move(p, motion, p_move):
    dy = motion[0]
    dx = motion[1]
    rows = len(p)
    cols = len(p[0])
    q = []
    for i in range(rows):
        # iterate Y
        q.append([])
        for j in range(cols):
            # iterage X
            q[i].append( p[ (i - dy) % rows ][ (j - dx) % cols ] * p_move + p[i][j] * (1 - p_move))
    return q

def measure(p, measurement, sensor_right, colors):
    rows = len(p)
    cols = len(p[0])
    total = rows*cols
    normalizer = 0

    q = []
    for i in range(rows):
        # iterate Y
        q.append([])
        for j in range(cols):
            # iterage X
            match = colors[i][j] == measurement
            q[i].append( p[i][j] * (match * sensor_right + (1 - match) * (1 - sensor_right)))

        normalizer += sum(q[i])

    # normalize
    for i in range(rows):
        for j in range(cols):
            q[i][j] /= normalizer

    return q

def calculate():

    #DO NOT USE IMPORT
    #ENTER CODE BELOW HERE
    #ANY CODE ABOVE WILL CAUSE
    #HOMEWORK TO BE GRADED
    #INCORRECT

    p = localize(colors,measurements,motions,sensor_right,p_move)

    #Your probability array must be printed
    #with the following code.

    show(p)
    return p

