# --------------
# USER INSTRUCTIONS
#
# Write a function called stochastic_value that
# takes no input and RETURNS two grids. The
# first grid, value, should contain the computed
# value of each cell as shown in the video. The
# second grid, policy, should contain the optimum
# policy for each cell.
#
# Stay tuned for a homework help video! This should
# be available by Thursday and will be visible
# in the course content tab.
#
# Good luck! Keep learning!
#
# --------------
# GRADING NOTES
#
# We will be calling your stochastic_value function
# with several different grids and different values
# of success_prob, collision_cost, and cost_step.
# In order to be marked correct, your function must
# RETURN (it does not have to print) two grids,
# value and policy.
#
# When grading your value grid, we will compare the
# value of each cell with the true value according
# to this model. If your answer for each cell
# is sufficiently close to the correct answer
# (within 0.001), you will be marked as correct.
#
# NOTE: Please do not modify the values of grid,
# success_prob, collision_cost, or cost_step inside
# your function. Doing so could result in your
# submission being inappropriately marked as incorrect.

# -------------
# GLOBAL VARIABLES
#
# You may modify these variables for testing
# purposes, but you should only modify them here.
# Do NOT modify them inside your stochastic_value
# function.

grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0]]

goal = [0, len(grid[0])-1] # Goal is in top right corner


delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>'] # Use these when creating your policy grid.

success_prob = 0.5
failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
collision_cost = 100
cost_step = 1


############## INSERT/MODIFY YOUR CODE BELOW ##################
#
# You may modify the code below if you want, but remember that
# your function must...
#
# 1) ...be called stochastic_value().
# 2) ...NOT take any arguments.
# 3) ...return two grids: FIRST value and THEN policy.

def stochastic_value(grid,goal,cost_step,collision_cost,success_prob):
    failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
    value = [[collision_cost for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]

    rows = len(grid)
    cols = len(grid[0])

    changed = True
    while changed:
        changed = False

        x = goal[0]
        y = goal[1]
        h = 0
        next = [[h, x, y]]

        visited = [[0 for col in range(len(grid[0]))] for row in range(len(grid))]
        while len(next) != 0:
            next.sort()
            currentCell = next.pop(0)

            x = currentCell[1]
            y = currentCell[2]
            h = currentCell[0]

            value[x][y] = h

            for i in range(len(delta)):
                currentDelta = delta[i]
                x2 = x - currentDelta[0]
                y2 = y - currentDelta[1]

                if (x2 >= 0 and y2 >= 0 and x2 < rows and y2 < cols and grid[x2][y2] != 1 and visited[x2][y2] == 0):
                    visited[x2][y2] = 1
                    h2 = cost_step

                    failDelta1 = delta[(i - 1) % 4]
                    failDelta2 = delta[(i + 1) % 4]

                    x3 = x2 + failDelta1[0]
                    y3 = y2 + failDelta1[1]
                    x4 = x2 + failDelta2[0]
                    y4 = y2 + failDelta2[1]

                    if (x3 >= 0 and y3 >= 0 and x3 < rows and y3 < cols):
                        failCost1 = value[x3, y3]
                    else:
                        failCost1 = collision_cost

                    if (x4 >= 0 and y4 >= 0 and x4 < rows and y4 < cols):
                        failCost2 = value[x4, y4]
                    else:
                        failCost2 = collision_cost

                    h2 += failCost1 * failure_prob
                    h2 += failCost2 * failure_prob

                    if (h2 < value[x2, y2]):
                        value[x2, y2] = h2
                        changed = True



    return value, policy

